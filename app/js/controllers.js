'use strict';
angular.module('app.controllers', []).
        controller('LoginCtrl', function ($scope, $rootScope, $user, $state, OAuth, OAuthToken, $cookies)
        {
            OAuthToken.removeToken();
            var cookies = $cookies.getAll();
            angular.forEach(cookies, function (v, k) {
                $cookies.remove(k);
            });
            $rootScope.isLoginPage = true;
            $rootScope.isLightLoginPage = false;
            $rootScope.isLockscreenPage = false;
            $rootScope.isMainPage = false;
            if (OAuth.isAuthenticated()) {
                $state.go('app.dashboard');
            }

            $scope.doLogin = function (data) {
                public_vars.$pageLoadingOverlay.removeClass('loaded');
                OAuth.getAccessToken(data).then(function (response) {
                    $user.getEntity();
                    $user.getPermissions().then(function () {
                        public_vars.$pageLoadingOverlay.addClass('loaded');
                        $state.go('app.dashboard');
                    });
                }, function (data) {
                    var opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    public_vars.$pageLoadingOverlay.addClass('loaded');
                    toastr.error("Usuário ou Senha incorretos, tente novamente", "Login inválido", opts);
                });
            };
        }).
        controller('LogoutCtrl', function ($scope, $rootScope, OAuthToken, $state, $cookies)
        {
            OAuthToken.removeToken();
            var cookies = $cookies.getAll();
            angular.forEach(cookies, function (v, k) {
                $cookies.remove(k);
            });
            $state.go('login');
        }).
        controller('ResourcesCtrl', function ($scope, $rootScope, $element, $http, URL, ngDialog, $cookies, $logger)
        {
            //Controller Vars
            $scope.sortType = 'id_resource';
            $scope.sortReverse = false;
            $scope.search = '';

            var userEntity = $cookies.getObject('userEntity');
            var userEmail = userEntity[0].username;

            $scope.formData = [];
            $scope.status = {
                0: "Inativo",
                1: "Ativo"
            };

            //Reload Panel Action
            $scope.reloadPanelAction = function () {
                getDataAction();
            };

            //Get Data Action
            var getDataAction = function () {
                var $panel = $($element);
                $panel.append('<div class="panel-disabled"><div class="loader-2"></div></div>');
                var $pd = $panel.find('.panel-disabled');
                $http.get(URL.api + 'resources').then(function (response) {
                    $http.get(URL.api + 'resources_type').then(function (responseTypes) {

                        $scope.resources_types = responseTypes.data._embedded.resources_type;
                        $scope.resources = response.data._embedded.resources;

                        angular.forEach($scope.resources, function (obj) {
                            obj.type_entity = $scope.resources_types.filter(function (subObj) {
                                return subObj.id_type == obj.id_type
                            });
                        });

                        $pd.fadeOut('slow', function ()
                        {
                            $pd.remove();
                        });
                    });
                }
                );
            };

            //Toggle Status Action
            $scope.toggleStatusAction = function (id_resource, $event) {
                $('#btn_resource_' + id_resource).prop("disabled", true);
                $('#btn_resource_' + id_resource).html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                $http.get(URL.api + 'resources/' + id_resource).then(function (responseGet) {
                    var resourceStatus = responseGet.data.active;
                    if (resourceStatus == 1) {
                        var dataPatch = {"active": 0}
                    } else {
                        var dataPatch = {"active": 1}
                    }

                    $http.patch(URL.api + 'resources/' + id_resource, dataPatch).then(function (response) {
                        var dataLog = {
                            user: userEmail,
                            entity: "resources",
                            action: "O usuário alterou a permissão " + id_resource,
                            sniffer: JSON.stringify(response)
                        }
                        $logger.update(dataLog);
                        var opts = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.success("Permissão alterada com sucesso", "Sucesso", opts);
                        $scope.reloadPanelAction();
                    });
                });
            };

            //Modal Insert Button Action
            $scope.openModalInsertAction = function () {
                ngDialog.open({
                    template: appHelper.templatePath('manage/resourceInsert'),
                    scope: $scope,
                    name: "insert"
                });
            };

            //Modal Edit Button Action
            $scope.openModalEditAction = function (id_resource) {
                $scope.workingId = id_resource;
                ngDialog.open({
                    template: appHelper.templatePath('manage/resourceEdit'),
                    scope: $scope,
                    name: "edit"
                });
            };

            //Modal Open Event
            $rootScope.$on('ngDialog.opened', function (e, $dialog) {
                var dialogType = $dialog.name;
                if (dialogType == "insert") {
                    $http.get(URL.api + 'resources_type').then(function (response) {
                        $scope.resources_type = response.data._embedded.resources_type;
                        $scope.formData.resources_type_selected = $scope.resources_type[0];
                        $('#span_resources_type_selected').html('');
                    });
                }
                else if (dialogType == "edit") {
                    // Put default values to inputs
                    $http.get(URL.api + 'resources/' + $scope.workingId).then(function (response) {

                        $scope.formData = {
                            name: response.data.resource_name,
                            description: response.data.resource_desc,
                            resources_type_selected: {
                                id_type: response.data.id_type
                            }
                        };
                        $('#span_name').html('');
                        $('#span_description').html('');
                    });

                    //Get All Types
                    $http.get(URL.api + 'resources_type').then(function (response) {


                        $scope.resources_type = response.data._embedded.resources_type;
                        $('#span_resources_type_selected').html('');
                    });
                }
            });

            //Modal Closed Event
            $rootScope.$on('ngDialog.closed', function (e, $dialog) {
                $scope.formData = {};
            });

            //Do Insert
            $scope.doInsert = function (formData) {
                $("#saveButton").prop("disabled", true);
                $("#saveLabel").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                var dataPost = {
                    resource_name: formData.name,
                    resource_desc: formData.description,
                    owner: userEmail,
                    id_type: formData.resources_type_selected.id_type
                }

                $http.post(URL.api + 'resources', dataPost).then(function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "O usuário inseriu uma nova permissão",
                        sniffer: JSON.stringify(response)
                    }

                    $logger.create(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Permissão inserida com sucesso", "Sucesso", opts);
                    $scope.reloadPanelAction();
                }, function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "Ocorreu um erro ao inserir a permissão",
                        sniffer: JSON.stringify(response)
                    }
                    $logger.error(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.error("Ocorreu um erro ao inserir a permissão", "Erro " + response.status, opts);
                    $scope.reloadPanelAction();
                });
            };

            //Do Update
            $scope.doUpdate = function (formData) {
                $("#editButton").prop("disabled", true);
                $("#editLabel").html('<i class="fa fa-circle-o-notch fa-spin"></i>');

                var dataPatch = {
                    resource_desc: formData.description,
                    owner: userEmail,
                    id_type: formData.resources_type_selected.id_type
                }

                $http.patch(URL.api + 'resources/' + $scope.workingId, dataPatch).then(function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "O usuário alterou uma permissão",
                        sniffer: JSON.stringify(response)
                    }

                    $logger.update(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Permissão alterada com sucesso", "Sucesso", opts);
                    $scope.reloadPanelAction();
                }, function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "Ocorreu um erro ao alterar a permissão",
                        sniffer: JSON.stringify(response)
                    }
                    $logger.error(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.error("Ocorreu um erro ao alterar a permissão", "Erro " + response.status, opts);
                    $scope.reloadPanelAction();
                });

            }

            getDataAction();
        }).
        controller('UsersCtrl', function ($scope, $rootScope, $element, $http, URL, ngDialog, $cookies, $logger)
        {
            //Controller Vars
            $scope.sortType = 'id_user';
            $scope.sortReverse = false;
            $scope.search = '';

            var userEntity = $cookies.getObject('userEntity');
            var userEmail = userEntity[0].username;

            $scope.formData = [];
            $scope.status = {
                0: "Inativo",
                1: "Ativo"
            };

            //Reload Panel Action
            $scope.reloadPanelAction = function () {
                getDataAction();
            };

            //Get Data Action
            var getDataAction = function () {
                var $panel = $($element);
                $panel.append('<div class="panel-disabled"><div class="loader-2"></div></div>');
                var $pd = $panel.find('.panel-disabled');
                $http.get(URL.api + 'resources').then(function (response) {

                    console.log(response);
                    $pd.fadeOut('slow', function ()
                    {
                        $pd.remove();
                    });
                }
                );
            };

            //Toggle Status Action
            $scope.toggleStatusAction = function (id_resource, $event) {
                $('#btn_resource_' + id_resource).prop("disabled", true);
                $('#btn_resource_' + id_resource).html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                $http.get(URL.api + 'resources/' + id_resource).then(function (responseGet) {
                    var resourceStatus = responseGet.data.active;
                    if (resourceStatus == 1) {
                        var dataPatch = {"active": 0}
                    } else {
                        var dataPatch = {"active": 1}
                    }

                    $http.patch(URL.api + 'resources/' + id_resource, dataPatch).then(function (response) {
                        var dataLog = {
                            user: userEmail,
                            entity: "resources",
                            action: "O usuário alterou a permissão " + id_resource,
                            sniffer: JSON.stringify(response)
                        }
                        $logger.update(dataLog);
                        var opts = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": true,
                            "positionClass": "toast-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.success("Permissão alterada com sucesso", "Sucesso", opts);
                        $scope.reloadPanelAction();
                    });
                });
            };

            //Modal Insert Button Action
            $scope.openModalInsertAction = function () {
                ngDialog.open({
                    template: appHelper.templatePath('manage/resourceInsert'),
                    scope: $scope,
                    name: "insert"
                });
            };

            //Modal Edit Button Action
            $scope.openModalEditAction = function (id_resource) {
                $scope.workingId = id_resource;
                ngDialog.open({
                    template: appHelper.templatePath('manage/resourceEdit'),
                    scope: $scope,
                    name: "edit"
                });
            };

            //Modal Open Event
            $rootScope.$on('ngDialog.opened', function (e, $dialog) {
                var dialogType = $dialog.name;
                if (dialogType == "insert") {
                    $http.get(URL.api + 'resources_type').then(function (response) {
                        $scope.resources_type = response.data._embedded.resources_type;
                        $scope.formData.resources_type_selected = $scope.resources_type[0];
                        $('#span_resources_type_selected').html('');
                    });
                }
                else if (dialogType == "edit") {
                    // Put default values to inputs
                    $http.get(URL.api + 'resources/' + $scope.workingId).then(function (response) {

                        $scope.formData = {
                            name: response.data.resource_name,
                            description: response.data.resource_desc,
                            resources_type_selected: {
                                id_type: response.data.id_type
                            }
                        };
                        $('#span_name').html('');
                        $('#span_description').html('');
                    });

                    //Get All Types
                    $http.get(URL.api + 'resources_type').then(function (response) {


                        $scope.resources_type = response.data._embedded.resources_type;
                        $('#span_resources_type_selected').html('');
                    });
                }
            });

            //Modal Closed Event
            $rootScope.$on('ngDialog.closed', function (e, $dialog) {
                $scope.formData = {};
            });

            //Do Insert
            $scope.doInsert = function (formData) {
                $("#editButton").prop("disabled", true);
                $("#editLabel").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                var dataPost = {
                    resource_name: formData.name,
                    resource_desc: formData.description,
                    owner: userEmail,
                    id_type: formData.resources_type_selected.id_type
                }

                $http.post(URL.api + 'resources', dataPost).then(function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "O usuário inseriu uma nova permissão",
                        sniffer: JSON.stringify(response)
                    }

                    $logger.create(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Permissão inserida com sucesso", "Sucesso", opts);
                    $scope.reloadPanelAction();
                }, function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "Ocorreu um erro ao inserir a permissão",
                        sniffer: JSON.stringify(response)
                    }
                    $logger.error(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.error("Ocorreu um erro ao inserir a permissão", "Erro " + response.status, opts);
                    $scope.reloadPanelAction();
                });
            };

            $scope.doUpdate = function (formData) {
                $("#editButton").prop("disabled", true);
                $("#editLabel").html('<i class="fa fa-circle-o-notch fa-spin"></i>');

                var dataPatch = {
                    resource_desc: formData.description,
                    owner: userEmail,
                    id_type: formData.resources_type_selected.id_type
                }

                $http.patch(URL.api + 'resources/' + $scope.workingId, dataPatch).then(function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "O usuário alterou uma permissão",
                        sniffer: JSON.stringify(response)
                    }

                    $logger.update(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Permissão alterada com sucesso", "Sucesso", opts);
                    $scope.reloadPanelAction();
                }, function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "Ocorreu um erro ao alterar a permissão",
                        sniffer: JSON.stringify(response)
                    }
                    $logger.error(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.error("Ocorreu um erro ao alterar a permissão", "Erro " + response.status, opts);
                    $scope.reloadPanelAction();
                });

            }

            getDataAction();
        }).
        controller('RoleGroupCtrl', function ($scope, $rootScope, $element, $http, URL, ngDialog, $cookies, $logger)
        {

            //Controller Vars
            $scope.sortType = 'id_group';
            $scope.sortReverse = false;
            $scope.search = '';

            var userEntity = $cookies.getObject('userEntity');
            var userEmail = userEntity[0].username;

            $scope.formData = [];
            $scope.status = {
                0: "Inativo",
                1: "Ativo"
            };

            //Reload Panel Action
            $scope.reloadPanelAction = function () {
                getDataAction();
            };

            //Get Data Action
            var getDataAction = function () {
                var $panel = $($element);
                $panel.append('<div class="panel-disabled"><div class="loader-2"></div></div>');
                var $pd = $panel.find('.panel-disabled');
                $http.get(URL.api + 'role_group').then(function (response) {
                    $scope.role_groups = response.data._embedded.role_group;
                    $pd.fadeOut('slow', function ()
                    {
                        $pd.remove();
                    });
                }
                );
            };

            //Modal Insert Button Action
            $scope.openModalInsertAction = function () {
                ngDialog.open({
                    template: appHelper.templatePath('manage/role-groupInsert'),
                    scope: $scope,
                    name: "insert"
                });
            };

            //Modal Edit Button Action
            $scope.openModalEditAction = function (id_group) {
                $scope.workingId = id_group;
                ngDialog.open({
                    template: appHelper.templatePath('manage/role-groupEdit'),
                    scope: $scope,
                    name: "edit"
                });
            };

            //Modal Open Event
            $rootScope.$on('ngDialog.opened', function (e, $dialog) {
                var dialogType = $dialog.name;
                if (dialogType == "insert") {
                    $('#selectContainer').hide();
                    $http.get(URL.api + 'resources?active=1').then(function (response) {
                        $('#selectSpinner').hide();
                        $('#selectContainer').show();
                        $scope.resources = response.data._embedded.resources;
                    });
                }
                else if (dialogType == "edit") {
                    // Put default values to inputs
                    $http.get(URL.api + 'resources/' + $scope.workingId).then(function (response) {

                        $scope.formData = {
                            name: response.data.resource_name,
                            description: response.data.resource_desc,
                            resources_type_selected: {
                                id_type: response.data.id_type
                            }
                        };
                        $('#span_name').html('');
                        $('#span_description').html('');
                    });

                    //Get All Types
                    $http.get(URL.api + 'resources_type').then(function (response) {


                        $scope.resources_type = response.data._embedded.resources_type;
                        $('#span_resources_type_selected').html('');
                    });
                }
            });

            //Modal Closed Event
            $rootScope.$on('ngDialog.closed', function (e, $dialog) {
                $scope.formData = {};
            });

            //Do Insert
            $scope.doInsert = function (formData) {
                $("#saveButton").prop("disabled", true);
                $("#saveLabel").html('<i class="fa fa-circle-o-notch fa-spin"></i>');

                var dataPost = {
                    groupname: formData.name,
                    owner: userEmail,
                    resources: formData.resources_selected
                }

                console.log(dataPost);

                $http.post(URL.api + 'insert-role-group', dataPost).then(function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "role_group",
                        action: "O usuário inseriu um novo grupo de permissões",
                        sniffer: JSON.stringify(response)
                    }

                    $logger.create(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Grupo inserido com sucesso", "Sucesso", opts);
                    $scope.reloadPanelAction();
                }, function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "role_group",
                        action: "Ocorreu um erro ao inserir o grupo de permissões",
                        sniffer: JSON.stringify(response)
                    }
                    $logger.error(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.error("Ocorreu um erro ao inserir o Grupo", "Erro " + response.status, opts);
                    $scope.reloadPanelAction();
                });
            };

            $scope.doUpdate = function (formData) {
                $("#editButton").prop("disabled", true);
                $("#editLabel").html('<i class="fa fa-circle-o-notch fa-spin"></i>');

                var dataPatch = {
                    resource_desc: formData.description,
                    owner: userEmail,
                    id_type: formData.resources_type_selected.id_type
                }

                $http.patch(URL.api + 'resources/' + $scope.workingId, dataPatch).then(function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "O usuário alterou uma permissão",
                        sniffer: JSON.stringify(response)
                    }

                    $logger.update(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("Permissão alterada com sucesso", "Sucesso", opts);
                    $scope.reloadPanelAction();
                }, function (response) {
                    var dataLog = {
                        user: userEmail,
                        entity: "resources",
                        action: "Ocorreu um erro ao alterar a permissão",
                        sniffer: JSON.stringify(response)
                    }
                    $logger.error(dataLog);
                    ngDialog.close();
                    var opts = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.error("Ocorreu um erro ao alterar a permissão", "Erro " + response.status, opts);
                    $scope.reloadPanelAction();
                });

            }

            getDataAction();
        }).
        controller('NotAllowedCtrl', function ($scope, $rootScope)
        {
            $rootScope.isLoginPage = false;
            $rootScope.isLightLoginPage = false;
            $rootScope.isLockscreenPage = false;
            $rootScope.isMainPage = false;
        }).
        controller('LoginLightCtrl', function ($scope, $rootScope)
        {
            $rootScope.isLoginPage = true;
            $rootScope.isLightLoginPage = true;
            $rootScope.isLockscreenPage = false;
            $rootScope.isMainPage = false;
        }).
        controller('LockscreenCtrl', function ($scope, $rootScope)
        {
            $rootScope.isLoginPage = false;
            $rootScope.isLightLoginPage = false;
            $rootScope.isLockscreenPage = true;
            $rootScope.isMainPage = false;
        }).
        controller('MainCtrl', function ($scope, $rootScope, $location, $layout, $layoutToggles, $pageLoadingBar, Fullscreen)
        {
            $rootScope.isLoginPage = false;
            $rootScope.isLightLoginPage = false;
            $rootScope.isLockscreenPage = false;
            $rootScope.isMainPage = true;
            $rootScope.layoutOptions = {
                horizontalMenu: {
                    isVisible: false,
                    isFixed: true,
                    minimal: false,
                    clickToExpand: false,
                    isMenuOpenMobile: false
                },
                sidebar: {
                    isVisible: true,
                    isCollapsed: true,
                    toggleOthers: true,
                    isFixed: true,
                    isRight: false,
                    isMenuOpenMobile: false,
                    // Added in v1.3
                    userProfile: true
                },
                chat: {
                    isOpen: false,
                },
                settingsPane: {
                    isOpen: false,
                    useAnimation: true
                },
                container: {
                    isBoxed: false
                },
                skins: {
                    sidebarMenu: '',
                    horizontalMenu: '',
                    userInfoNavbar: ''
                },
                pageTitles: true,
                userInfoNavVisible: false
            };
            //            $layout.loadOptionsFromCookies(); // remove this line if you don't want to support cookies that remember layout changes


            $scope.updatePsScrollbars = function ()
            {
                var $scrollbars = jQuery(".ps-scrollbar:visible");
                $scrollbars.each(function (i, el)
                {
                    if (typeof jQuery(el).data('perfectScrollbar') == 'undefined')
                    {
                        jQuery(el).perfectScrollbar();
                    }
                    else
                    {
                        jQuery(el).perfectScrollbar('update');
                    }
                })
            };
            // Define Public Vars
            public_vars.$body = jQuery("body");
            // Init Layout Toggles
            $layoutToggles.initToggles();
            // Other methods
            $scope.setFocusOnSearchField = function ()
            {
                public_vars.$body.find('.search-form input[name="s"]').focus();
                setTimeout(function () {
                    public_vars.$body.find('.search-form input[name="s"]').focus()
                }, 100);
            };
            // Watch changes to replace checkboxes
            $scope.$watch(function ()
            {
                cbr_replace();
            });
            // Watch sidebar status to remove the psScrollbar
            $rootScope.$watch('layoutOptions.sidebar.isCollapsed', function (newValue, oldValue)
            {
                if (newValue != oldValue)
                {
                    if (newValue == true)
                    {
                        public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar('destroy')
                    }
                    else
                    {
                        public_vars.$sidebarMenu.find('.sidebar-menu-inner').perfectScrollbar({wheelPropagation: public_vars.wheelPropagation});
                    }
                }
            });
            // Page Loading Progress (remove/comment this line to disable it)
            $pageLoadingBar.init();
            $scope.showLoadingBar = showLoadingBar;
            $scope.hideLoadingBar = hideLoadingBar;
            // Set Scroll to 0 When page is changed
            $rootScope.$on('$stateChangeStart', function ()
            {
                var obj = {pos: jQuery(window).scrollTop()};
                TweenLite.to(obj, .25, {pos: 0, ease: Power4.easeOut, onUpdate: function ()
                    {
                        $(window).scrollTop(obj.pos);
                    }});
            });
            // Full screen feature added in v1.3
            $scope.isFullscreenSupported = Fullscreen.isSupported();
            $scope.isFullscreen = Fullscreen.isEnabled() ? true : false;
            $scope.goFullscreen = function ()
            {
                if (Fullscreen.isEnabled())
                    Fullscreen.cancel();
                else
                    Fullscreen.all();
                $scope.isFullscreen = Fullscreen.isEnabled() ? true : false;
            }

        }).
        controller('SidebarMenuCtrl', function ($scope, $rootScope, $menuItems, $timeout, $location, $state, $layout)
        {
            // Menu Items
            var $sidebarMenuItems = $menuItems.instantiate();
            $scope.menuItems = $sidebarMenuItems.prepareSidebarMenu().getAll();
            // Set Active Menu Item
            $sidebarMenuItems.setActive($location.path());
            $rootScope.$on('$stateChangeSuccess', function ()
            {
                $sidebarMenuItems.setActive($state.current.name);
            });
            // Trigger menu setup
            public_vars.$sidebarMenu = public_vars.$body.find('.sidebar-menu');
            $timeout(setup_sidebar_menu, 1);
            ps_init(); // perfect scrollbar for sidebar
        }).
        controller('HorizontalMenuCtrl', function ($scope, $rootScope, $menuItems, $timeout, $location, $state)
        {
            var $horizontalMenuItems = $menuItems.instantiate();
            $scope.menuItems = $horizontalMenuItems.prepareHorizontalMenu().getAll();
            // Set Active Menu Item
            $horizontalMenuItems.setActive($location.path());
            $rootScope.$on('$stateChangeSuccess', function ()
            {
                $horizontalMenuItems.setActive($state.current.name);
                $(".navbar.horizontal-menu .navbar-nav .hover").removeClass('hover'); // Close Submenus when item is selected
            });
            // Trigger menu setup
            $timeout(setup_horizontal_menu, 1);
        }).
        controller('SettingsPaneCtrl', function ($rootScope)
        {
            // Define Settings Pane Public Variable
            public_vars.$settingsPane = public_vars.$body.find('.settings-pane');
            public_vars.$settingsPaneIn = public_vars.$settingsPane.find('.settings-pane-inner');
        }).
        controller('UIModalsCtrl', function ($scope, $rootScope, $modal, $sce)
        {
            // Open Simple Modal
            $scope.openModal = function (modal_id, modal_size, modal_backdrop)
            {
                $rootScope.currentModal = $modal.open({
                    templateUrl: modal_id,
                    size: modal_size,
                    backdrop: typeof modal_backdrop == 'undefined' ? true : modal_backdrop
                });
            };
            // Loading AJAX Content
            $scope.openAjaxModal = function (modal_id, url_location)
            {
                $rootScope.currentModal = $modal.open({
                    templateUrl: modal_id,
                    resolve: {
                        ajaxContent: function ($http)
                        {
                            return $http.get(url_location).then(function (response) {
                                $rootScope.modalContent = $sce.trustAsHtml(response.data);
                            }, function (response) {
                                $rootScope.modalContent = $sce.trustAsHtml('<div class="label label-danger">Cannot load ajax content! Please check the given url.</div>');
                            });
                        }
                    }
                });
                $rootScope.modalContent = $sce.trustAsHtml('Modal content is loading...');
            }
        }).
        controller('PaginationDemoCtrl', function ($scope)
        {
            $scope.totalItems = 64;
            $scope.currentPage = 4;
            $scope.setPage = function (pageNo)
            {
                $scope.currentPage = pageNo;
            };
            $scope.pageChanged = function ()
            {
                console.log('Page changed to: ' + $scope.currentPage);
            };
            $scope.maxSize = 5;
            $scope.bigTotalItems = 175;
            $scope.bigCurrentPage = 1;
        }).
        controller('LayoutVariantsCtrl', function ($scope, $layout, $cookies)
        {
            $scope.opts = {
                sidebarType: null,
                fixedSidebar: null,
                sidebarToggleOthers: null,
                sidebarVisible: null,
                sidebarPosition: null,
                horizontalVisible: null,
                fixedHorizontalMenu: null,
                horizontalOpenOnClick: null,
                minimalHorizontalMenu: null,
                sidebarProfile: null
            };
            $scope.sidebarTypes = [
                {value: ['sidebar.isCollapsed', false], text: 'Expanded', selected: $layout.is('sidebar.isCollapsed', false)},
                {value: ['sidebar.isCollapsed', true], text: 'Collapsed', selected: $layout.is('sidebar.isCollapsed', true)},
            ];
            $scope.fixedSidebar = [
                {value: ['sidebar.isFixed', true], text: 'Fixed', selected: $layout.is('sidebar.isFixed', true)},
                {value: ['sidebar.isFixed', false], text: 'Static', selected: $layout.is('sidebar.isFixed', false)},
            ];
            $scope.sidebarToggleOthers = [
                {value: ['sidebar.toggleOthers', true], text: 'Yes', selected: $layout.is('sidebar.toggleOthers', true)},
                {value: ['sidebar.toggleOthers', false], text: 'No', selected: $layout.is('sidebar.toggleOthers', false)},
            ];
            $scope.sidebarVisible = [
                {value: ['sidebar.isVisible', true], text: 'Visible', selected: $layout.is('sidebar.isVisible', true)},
                {value: ['sidebar.isVisible', false], text: 'Hidden', selected: $layout.is('sidebar.isVisible', false)},
            ];
            $scope.sidebarPosition = [
                {value: ['sidebar.isRight', false], text: 'Left', selected: $layout.is('sidebar.isRight', false)},
                {value: ['sidebar.isRight', true], text: 'Right', selected: $layout.is('sidebar.isRight', true)},
            ];
            $scope.horizontalVisible = [
                {value: ['horizontalMenu.isVisible', true], text: 'Visible', selected: $layout.is('horizontalMenu.isVisible', true)},
                {value: ['horizontalMenu.isVisible', false], text: 'Hidden', selected: $layout.is('horizontalMenu.isVisible', false)},
            ];
            $scope.fixedHorizontalMenu = [
                {value: ['horizontalMenu.isFixed', true], text: 'Fixed', selected: $layout.is('horizontalMenu.isFixed', true)},
                {value: ['horizontalMenu.isFixed', false], text: 'Static', selected: $layout.is('horizontalMenu.isFixed', false)},
            ];
            $scope.horizontalOpenOnClick = [
                {value: ['horizontalMenu.clickToExpand', false], text: 'No', selected: $layout.is('horizontalMenu.clickToExpand', false)},
                {value: ['horizontalMenu.clickToExpand', true], text: 'Yes', selected: $layout.is('horizontalMenu.clickToExpand', true)},
            ];
            $scope.minimalHorizontalMenu = [
                {value: ['horizontalMenu.minimal', false], text: 'No', selected: $layout.is('horizontalMenu.minimal', false)},
                {value: ['horizontalMenu.minimal', true], text: 'Yes', selected: $layout.is('horizontalMenu.minimal', true)},
            ];
            $scope.chatVisibility = [
                {value: ['chat.isOpen', false], text: 'No', selected: $layout.is('chat.isOpen', false)},
                {value: ['chat.isOpen', true], text: 'Yes', selected: $layout.is('chat.isOpen', true)},
            ];
            $scope.boxedContainer = [
                {value: ['container.isBoxed', false], text: 'No', selected: $layout.is('container.isBoxed', false)},
                {value: ['container.isBoxed', true], text: 'Yes', selected: $layout.is('container.isBoxed', true)},
            ];
            $scope.sidebarProfile = [
                {value: ['sidebar.userProfile', false], text: 'No', selected: $layout.is('sidebar.userProfile', false)},
                {value: ['sidebar.userProfile', true], text: 'Yes', selected: $layout.is('sidebar.userProfile', true)},
            ];
            $scope.resetOptions = function ()
            {
                $layout.resetCookies();
                window.location.reload();
            };
            var setValue = function (val)
            {
                if (val != null)
                {
                    val = eval(val);
                    $layout.setOptions(val[0], val[1]);
                }
            };
            $scope.$watch('opts.sidebarType', setValue);
            $scope.$watch('opts.fixedSidebar', setValue);
            $scope.$watch('opts.sidebarToggleOthers', setValue);
            $scope.$watch('opts.sidebarVisible', setValue);
            $scope.$watch('opts.sidebarPosition', setValue);
            $scope.$watch('opts.horizontalVisible', setValue);
            $scope.$watch('opts.fixedHorizontalMenu', setValue);
            $scope.$watch('opts.horizontalOpenOnClick', setValue);
            $scope.$watch('opts.minimalHorizontalMenu', setValue);
            $scope.$watch('opts.chatVisibility', setValue);
            $scope.$watch('opts.boxedContainer', setValue);
            $scope.$watch('opts.sidebarProfile', setValue);
        }).
        controller('ThemeSkinsCtrl', function ($scope, $layout)
        {
            var $body = jQuery("body");
            $scope.opts = {
                sidebarSkin: $layout.get('skins.sidebarMenu'),
                horizontalMenuSkin: $layout.get('skins.horizontalMenu'),
                userInfoNavbarSkin: $layout.get('skins.userInfoNavbar')
            };
            $scope.skins = [
                {value: '', name: 'Default', palette: ['#2c2e2f', '#EEEEEE', '#FFFFFF', '#68b828', '#27292a', '#323435']},
                {value: 'aero', name: 'Aero', palette: ['#558C89', '#ECECEA', '#FFFFFF', '#5F9A97', '#558C89', '#255E5b']},
                {value: 'navy', name: 'Navy', palette: ['#2c3e50', '#a7bfd6', '#FFFFFF', '#34495e', '#2c3e50', '#ff4e50']},
                {value: 'facebook', name: 'Facebook', palette: ['#3b5998', '#8b9dc3', '#FFFFFF', '#4160a0', '#3b5998', '#8b9dc3']},
                {value: 'turquoise', name: 'Truquoise', palette: ['#16a085', '#96ead9', '#FFFFFF', '#1daf92', '#16a085', '#0f7e68']},
                {value: 'lime', name: 'Lime', palette: ['#8cc657', '#ffffff', '#FFFFFF', '#95cd62', '#8cc657', '#70a93c']},
                {value: 'green', name: 'Green', palette: ['#27ae60', '#a2f9c7', '#FFFFFF', '#2fbd6b', '#27ae60', '#1c954f']},
                {value: 'purple', name: 'Purple', palette: ['#795b95', '#c2afd4', '#FFFFFF', '#795b95', '#27ae60', '#5f3d7e']},
                {value: 'white', name: 'White', palette: ['#FFFFFF', '#666666', '#95cd62', '#EEEEEE', '#95cd62', '#555555']},
                {value: 'concrete', name: 'Concrete', palette: ['#a8aba2', '#666666', '#a40f37', '#b8bbb3', '#a40f37', '#323232']},
                {value: 'watermelon', name: 'Watermelon', palette: ['#b63131', '#f7b2b2', '#FFFFFF', '#c03737', '#b63131', '#32932e']},
                {value: 'lemonade', name: 'Lemonade', palette: ['#f5c150', '#ffeec9', '#FFFFFF', '#ffcf67', '#f5c150', '#d9a940']},
            ];
            $scope.$watch('opts.sidebarSkin', function (val)
            {
                if (val != null)
                {
                    $layout.setOptions('skins.sidebarMenu', val);
                    $body.attr('class', $body.attr('class').replace(/\sskin-[a-z]+/)).addClass('skin-' + val);
                }
            });
            $scope.$watch('opts.horizontalMenuSkin', function (val)
            {
                if (val != null)
                {
                    $layout.setOptions('skins.horizontalMenu', val);
                    $body.attr('class', $body.attr('class').replace(/\shorizontal-menu-skin-[a-z]+/)).addClass('horizontal-menu-skin-' + val);
                }
            });
            $scope.$watch('opts.userInfoNavbarSkin', function (val)
            {
                if (val != null)
                {
                    $layout.setOptions('skins.userInfoNavbar', val);
                    $body.attr('class', $body.attr('class').replace(/\suser-info-navbar-skin-[a-z]+/)).addClass('user-info-navbar-skin-' + val);
                }
            });
        }).
        controller('FooterChatCtrl', function ($scope, $element)
        {
            $scope.isConversationVisible = false;
            $scope.toggleChatConversation = function ()
            {
                $scope.isConversationVisible = !$scope.isConversationVisible;
                if ($scope.isConversationVisible)
                {
                    setTimeout(function ()
                    {
                        var $el = $element.find('.ps-scrollbar');
                        if ($el.hasClass('ps-scroll-down'))
                        {
                            $el.scrollTop($el.prop('scrollHeight'));
                        }

                        $el.perfectScrollbar({
                            wheelPropagation: false
                        });
                        $element.find('.form-control').focus();
                    }, 300);
                }
            }
        });