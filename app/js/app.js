'use strict';
var app = angular.module('app', [
    'ngCookies',
    'ui.router',
    'ui.bootstrap',
    'oc.lazyLoad',
    'app.factory',
    'app.services',
    'app.controllers',
    'app.directives',
    'angular-oauth2',
    'FBAngular',
    'ngDialog',
    'frapontillo.bootstrap-duallistbox'
]);
app.run(function ($rootScope, OAuth, $state, $cookies)
{

    public_vars.$pageLoadingOverlay = jQuery('.page-loading-overlay');

    jQuery(window).load(function ()
    {
        public_vars.$pageLoadingOverlay.addClass('loaded');
    });

    $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                if (toState.name != 'login') {
                    if (!OAuth.isAuthenticated()) {
                        event.preventDefault();
                        $state.go('login');
                    }
                }

                if (toState.data.permission) {

                    var permissions = [];
                    angular.forEach($cookies.getObject('permissions').resources, function (value, key) {
                        permissions.push(Number(value.resource));
                    });

                    if (permissions.indexOf(toState.data.permission) == -1) {
                        event.preventDefault();
                        $state.go('app.notAllowed');
                    }
                }
            });
});

app.config(function (
        $provide,
        $stateProvider,
        $urlRouterProvider,
        $httpProvider,
        $ocLazyLoadProvider,
        OAuthProvider,
        OAuthTokenProvider,
        ASSETS,
        URL) {

    $httpProvider.interceptors.push(function ($q, $location, $cookies) {
        return {
            'responseError': function (response) {
                if (response.status == 401) {
                    var cookies = $cookies.getAll();
                    angular.forEach(cookies, function (v, k) {
                        $cookies.remove(k);
                    });
                    $location.path('/login');
                }
                return $q.reject(response);
            }
        };
    });

    OAuthProvider.configure({
        baseUrl: URL.api,
        clientId: 'webApp',
        clientSecret: 'webApp',
        grantPath: '/oauth',
        revokePath: '/oauth'
    });
    OAuthTokenProvider.configure({
        name: 'token',
        options: {
            secure: false
        }
    });
    $urlRouterProvider.otherwise('/login');
    $stateProvider

            //APP
            .state('login', {
                url: '/login',
                templateUrl: appHelper.templatePath('login'),
                controller: 'LoginCtrl',
                resolve: {
                    resources: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            ASSETS.extra.toastr
                        ]);
                    }
                },
                data: {},
            })
            .state('logout', {
                url: '/logout',
                controller: 'LogoutCtrl',
                data: {},
            })
            .state('app', {
                abstract: true,
                url: '/app',
                templateUrl: appHelper.templatePath('layout/app-body'),
                controller: function ($rootScope) {
                    $rootScope.isLoginPage = false;
                    $rootScope.isLightLoginPage = false;
                    $rootScope.isLockscreenPage = false;
                    $rootScope.isMainPage = true;
                }
            })
            .state('app.dashboard', {
                url: '/dashboard',
                templateUrl: appHelper.templatePath('dashboard/dashboard'),
                resolve: {
                    resources: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            ASSETS.charts.dxGlobalize,
                            ASSETS.extra.toastr,
                        ]);
                    },
                    dxCharts: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            ASSETS.charts.dxCharts,
                        ]);
                    },
                },
                data: {permission: 1},
            })

            //MANAGE
            .state('app.resources', {
                url: '/resources',
                templateUrl: appHelper.templatePath('manage/resources'),
                resolve: {
                    resources: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            ASSETS.extra.toastr
                        ]);
                    }
                },
                data: {permission: 1},
            })
            .state('app.roleGroups', {
                url: '/roleGroups',
                templateUrl: appHelper.templatePath('manage/role-groups'),
                resolve: {
                    resources: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            ASSETS.extra.toastr
                        ]);
                    }
                },
                data: {permission: 1},
            })
            .state('app.users', {
                url: '/users',
                templateUrl: appHelper.templatePath('manage/users'),
                resolve: {
                    resources: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            ASSETS.extra.toastr
                        ]);
                    }
                },
                data: {permission: 1},
            })

            //EXTRA
            .state('app.notAllowed', {
                url: '/not-allowed',
                templateUrl: appHelper.templatePath('extra/not-allowed'),
                controller: 'NotAllowedCtrl',
                data: {},
            });
});
app.constant(
        'ASSETS', {
            'core': {
                'bootstrap': appHelper.assetPath('js/bootstrap.min.js'), // Some plugins which do not support angular needs this

                'jQueryUI': [
                    appHelper.assetPath('js/jquery-ui/jquery-ui.min.js'),
                    appHelper.assetPath('js/jquery-ui/jquery-ui.structure.min.css'),
                ],
                'moment': appHelper.assetPath('js/moment.min.js'),
                'googleMapsLoader': appHelper.assetPath('app/js/angular-google-maps/load-google-maps.js')
            },
            'charts': {
                'dxGlobalize': appHelper.assetPath('js/devexpress-web-14.1/js/globalize.min.js'),
                'dxCharts': appHelper.assetPath('js/devexpress-web-14.1/js/dx.chartjs.js'),
                'dxVMWorld': appHelper.assetPath('js/devexpress-web-14.1/js/vectormap-data/world.js'),
            },
            'xenonLib': {
                notes: appHelper.assetPath('js/xenon-notes.js'),
            },
            'maps': {
                'vectorMaps': [
                    appHelper.assetPath('js/jvectormap/jquery-jvectormap-1.2.2.min.js'),
                    appHelper.assetPath('js/jvectormap/regions/jquery-jvectormap-world-mill-en.js'),
                    appHelper.assetPath('js/jvectormap/regions/jquery-jvectormap-it-mill-en.js'),
                ],
            },
            'icons': {
                'meteocons': appHelper.assetPath('css/fonts/meteocons/css/meteocons.css'),
                'elusive': appHelper.assetPath('css/fonts/elusive/css/elusive.css'),
            },
            'forms': {
                'select2': [
                    appHelper.assetPath('js/select2/select2.css'),
                    appHelper.assetPath('js/select2/select2-bootstrap.css'),
                    appHelper.assetPath('js/select2/select2.min.js'),
                ],
                'daterangepicker': [
                    appHelper.assetPath('js/daterangepicker/daterangepicker-bs3.css'),
                    appHelper.assetPath('js/daterangepicker/daterangepicker.js'),
                ],
                'colorpicker': appHelper.assetPath('js/colorpicker/bootstrap-colorpicker.min.js'),
                'selectboxit': appHelper.assetPath('js/selectboxit/jquery.selectBoxIt.js'),
                'tagsinput': appHelper.assetPath('js/tagsinput/bootstrap-tagsinput.min.js'),
                'datepicker': appHelper.assetPath('js/datepicker/bootstrap-datepicker.js'),
                'timepicker': appHelper.assetPath('js/timepicker/bootstrap-timepicker.min.js'),
                'inputmask': appHelper.assetPath('js/inputmask/jquery.inputmask.bundle.js'),
                'formWizard': appHelper.assetPath('js/formwizard/jquery.bootstrap.wizard.min.js'),
                'jQueryValidate': appHelper.assetPath('js/jquery-validate/jquery.validate.min.js'),
                'dropzone': [
                    appHelper.assetPath('js/dropzone/css/dropzone.css'),
                    appHelper.assetPath('js/dropzone/dropzone.min.js'),
                ],
                'typeahead': [
                    appHelper.assetPath('js/typeahead.bundle.js'),
                    appHelper.assetPath('js/handlebars.min.js'),
                ],
                'multiSelect': [
                    appHelper.assetPath('bower_components/angularjs-dropdown-multiselect/dist/angularjs-dropdown-multiselect.min.js'),
                ],
                'icheck': [
                    appHelper.assetPath('js/icheck/skins/all.css'),
                    appHelper.assetPath('js/icheck/icheck.min.js'),
                ],
                'bootstrapWysihtml5': [
                    appHelper.assetPath('js/wysihtml5/src/bootstrap-wysihtml5.css'),
                    appHelper.assetPath('js/wysihtml5/wysihtml5-angular.js')
                ],
            },
            'uikit': {
                'base': [
                    appHelper.assetPath('js/uikit/uikit.css'),
                    appHelper.assetPath('js/uikit/css/addons/uikit.almost-flat.addons.min.css'),
                    appHelper.assetPath('js/uikit/js/uikit.min.js'),
                ],
                'codemirror': [
                    appHelper.assetPath('js/uikit/vendor/codemirror/codemirror.js'),
                    appHelper.assetPath('js/uikit/vendor/codemirror/codemirror.css'),
                ],
                'marked': appHelper.assetPath('js/uikit/vendor/marked.js'),
                'htmleditor': appHelper.assetPath('js/uikit/js/addons/htmleditor.min.js'),
                'nestable': appHelper.assetPath('js/uikit/js/addons/nestable.min.js'),
            },
            'extra': {
                'tocify': appHelper.assetPath('js/tocify/jquery.tocify.min.js'),
                'toastr': appHelper.assetPath('bower_components/toastr/toastr.min.js'),
                'fullCalendar': [
                    appHelper.assetPath('js/fullcalendar/fullcalendar.min.css'),
                    appHelper.assetPath('js/fullcalendar/fullcalendar.min.js'),
                ],
                'cropper': [
                    appHelper.assetPath('js/cropper/cropper.min.js'),
                    appHelper.assetPath('js/cropper/cropper.min.css'),
                ]
            }
        }
);
app.constant(
        'URL', {
//            'api': 'http://stage.trend2b.net/philips/channel/public/'
            'api': 'http://philips-api.local/'
        }
);