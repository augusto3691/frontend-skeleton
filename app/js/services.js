'use strict';
angular.module('app.services', []).
        service('$menuItems', function ($cookies)
        {
            this.menuItems = [];
            var permissions = [];
            var $menuItemsRef = this;
            var menuItemObj = {
                parent: null,
                title: '',
                link: '', // starting with "./" will refer to parent link concatenation
                state: '', // will be generated from link automatically where "/" (forward slashes) are replaced with "."
                icon: '',
                permission: null,
                isActive: false,
                label: null,
                menuItems: [],
                setLabel: function (label, color, hideWhenCollapsed)
                {
                    if (typeof hideWhenCollapsed == 'undefined')
                        hideWhenCollapsed = true;
                    this.label = {
                        text: label,
                        classname: color,
                        collapsedHide: hideWhenCollapsed
                    };
                    return this;
                },
                addItem: function (title, link, icon, permission)
                {
                    var parent = this,
                            item = angular.extend(angular.copy(menuItemObj), {
                                parent: parent,
                                title: title,
                                link: link,
                                icon: icon,
                                permission: permission
                            });
                    if (item.link)
                    {
                        if (item.link.match(/^\./))
                            item.link = parent.link + item.link.substring(1, link.length);
                        if (item.link.match(/^-/))
                            item.link = parent.link + '-' + item.link.substring(2, link.length);
                        item.state = $menuItemsRef.toStatePath(item.link);
                    }
                    if (permissions.indexOf(permission) !== -1) {
                        this.menuItems.push(item);
                    }
                    return item;
                }
            };
            this.addItem = function (title, link, icon, permission)
            {
                var item = angular.extend(angular.copy(menuItemObj), {
                    title: title,
                    link: link,
                    state: this.toStatePath(link),
                    icon: icon,
                    permission: permission
                });
                if (permissions.indexOf(permission) !== -1) {
                    this.menuItems.push(item);
                }


                return item;
            };
            this.getAll = function ()
            {
                return this.menuItems;
            };
            this.prepareSidebarMenu = function ()
            {
                angular.forEach($cookies.getObject('permissions').resources, function (value, key) {
                    permissions.push(Number(value.resource));
                });

                var dashboard = this.addItem('Dashboard', '/app/dashboard', 'linecons-cog', 1);
                var manage = this.addItem('Gerenciar', '', 'fa fa-cogs', 1);

                var manageSub =
                        manage.addItem('Permissões', '/app/resources', 'fa fa-lock', 1);
                manage.addItem('Perfis', '/app/roleGroups', 'fa fa-users', 1);
                manage.addItem('Usuários', '/app/users', 'fa fa-user', 1);

                return this;
            };
            this.prepareHorizontalMenu = function ()
            {
                // return prepareSidebarMenu;
                return this;
            }

            this.instantiate = function ()
            {
                return angular.copy(this);
            }

            this.toStatePath = function (path)
            {
                return path.replace(/\//g, '.').replace(/^\./, '');
            };
            this.setActive = function (path)
            {
                this.iterateCheck(this.menuItems, this.toStatePath(path));
            };
            this.setActiveParent = function (item)
            {
                item.isActive = true;
                item.isOpen = true;
                if (item.parent)
                    this.setActiveParent(item.parent);
            };
            this.iterateCheck = function (menuItems, currentState)
            {
                angular.forEach(menuItems, function (item)
                {
                    if (item.state == currentState)
                    {
                        item.isActive = true;
                        if (item.parent != null)
                            $menuItemsRef.setActiveParent(item.parent);
                    }
                    else
                    {
                        item.isActive = false;
                        item.isOpen = false;
                        if (item.menuItems.length)
                        {
                            $menuItemsRef.iterateCheck(item.menuItems, currentState);
                        }
                    }
                });
            }
        });